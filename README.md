# Arqtan.io Kubernetes Operator Example
This repository is intended to serve as an example of how to use `kubebuilder` to generate the scaffold for a native Golang Kubernetes Operator along with associated CRD manifests and so on.

## Description
The operator watches for new `WhaleSay` custom resources; this is custom resource that for the purpose of this example wraps the [Docker image](https://hub.docker.com/r/docker/whalesay/), which in turn wraps the `cowsay` command which simply writes a amusing ASCII art to the logs along with a custom message.

## Development
You’ll need a Kubernetes cluster to run against, most likely you will use
[Minikube](https://minikube.sigs.k8s.io/docs/) for this purpose, however, you 
can alternatively use [KIND](https://sigs.k8s.io/kind) or 
[Docker Desktop](https://www.docker.com/products/docker-desktop/) (indeed you
might already have the latter installed and all you need to do is enable
Kubernetes in the settings).

### Toolchain & Libraries
The project boilerplate and layout is defined and managed by 
[Kubebuilder](https://book.kubebuilder.io/quick-start.html), and the original
bootstrapping steps used at the project inception are documented below.  If you 
wish to add new APIs or Webhooks you will also need to get this installed, see
[Creating a New API](#creating-a-new-api) below.  

Additionally, it is worth noting that Kubebuilder is now also a sub-component of
the [Operator SDK](https://sdk.operatorframework.io/) which is in-turn part of 
the [CCNF](https://www.cncf.io/projects/operator-framework/).

Both [Operator SDK](https://sdk.operatorframework.io/docs/) and 
[Kubebuilder](https://book.kubebuilder.io/) have good documentation, and it is
worth noting that there is no mention in the Kubebuilder documentation of
Operator SDK, but they are, in-fact, compatible as explained [here](https://sdk.operatorframework.io/docs/faqs/#what-are-the-the-differences-between-kubebuilder-and-operator-sdk).

All other tool installations are managed automatically by the `Makefile`
scaffolded by Kubebuiler.  The `Makefile` will download the binaries it needs
into the `bin` subdirectory (which is ignored by `.gitignore`). However, for
reference these are the tools it uses:

 - [Kustomize](https://kustomize.io/) for YAML based templation
 - [`controller-gen`](https://book.kubebuilder.io/reference/controller-gen.html)
   for some of the Golang boilerplate
 - [`envtest`](https://pkg.go.dev/sigs.k8s.io/controller-runtime/pkg/envtest) 
   for running tests

Additionally, familarity with the following Golang libraries will also be very
helpful:

 - [`controller-runtime`](https://pkg.go.dev/sigs.k8s.io/controller-runtime)
   wraps a lot of the scaffold code need to register and start controllers.
 - [`apimachinery`](https://pkg.go.dev/k8s.io/apimachinery) contains a lot of
   helpers for interacting with the Kubernetes API and base kinds.
 - [`client-go`](https://pkg.go.dev/k8s.io/client-go) has a lot of the client
   helpers for talking to Kubernetes clusters; a lot of this, but not all, is
   wrapped by 
   [`controller-runtime`](https://pkg.go.dev/sigs.k8s.io/controller-runtime).


### Running Operator Locally
Assuming we are using a local cluster like 
[Minikube](https://minikube.sigs.k8s.io/docs/):

1. Delete your Minikube cluster if you have one:
   ```
   minikube delete
   ```

2. Recreate the cluster from scratch:
   ```
   minikube start
   ```

3. Install the CRDs onto the cluster:
   ```
   make install
   ```

4. Run the operator process locally:
   ```
   make run
   ```

5. *Optional:* In a different terminal, install the sample CRs:
   ```
   kubectl apply -f config/samples/
   ```

*Note:* its possible to combine steps 4. and 5. with `make install run`.
   
### Debugging in VSCode
The repository comes with a `.vscode/launch.json` that includes a launch target
for debugging the controller code directly inside VSCode, to do this:

1. Run steps 1. to 3. from [Running Operator Locally](#running-operator-locally) 
   above if you have not already.

2. In VSCode set a breakpoint inside the controller `Reconcile` function you
   wish to debug, for example, to debug the `WhaleSay` kind, then set your
   breakpoint in `controllers/whalesay_controller.go`.

3. Using the VSCode debug tab, run the "Debug operator" configuration and wait
   for the Debug console to show the process has started.

4. In a different terminal, create a CR corresponding to the controller you wish
   to debug, for example to trigger the breakpoint above run the following:
   ```
   kubectl apply -f config/samples/_v1_whalesay.yaml
   ```

### Modifying the API
If you edit the API definitions in the `api/**/*_types.go` files then it will be
required that the manifests are re-generated and reinstalled to the cluster:

1. If required, remove the old manifests before they change:
   ```
   make uninstall
   ```

2. Re-generate the manifests against the new definitions
   ```
   make manifests
   ```

3. Install the new manifests to the cluster:
   ```
   make install
   ```

### Creating a New API
To create the boilerplate for a new API, we will need to use the
[Kubebuilder](https://book.kubebuilder.io/quick-start.html) CLI tool, so ensure
this is installed.

Kubebuilder will create all the scaffold needed for the new kind and the
contoller itself using the following command:
```
kubebuilder create api --group arqtan.io --version <version> --kind <NewKind>
```
In most cases `<version>` will start as `v1alpha1` and `<NewKind>` should follow
Kubernetes convention and be camelcapped.

Once you have run through this process then the CR spec and status can be edited
in the following auto-generated file:
```
api/<version>/<newkind>_types.go
```
And the controller `Reconcile` logic can be edited here:
```
controllers/<newkind>_controller.go
```

## Deploying
In order to deploy the controller binary to a Kubernetes cluster, Kubebuilder
provides us some helpers to achieve this. 

### Manual Deployment
As already mentioned it is possible to run the controller inside a cluster
using the Make targets Kubebuilder provided for us.  This is very convenient
for simple development tasks, however, to deploy to our integration and
production environments we should instead use our CI/CD pipeline as described
in the next section.  If you would like to run manually, here are the steps:

1. Run steps 2. to 4. from [Running Operator Locally](#running-operator-locally) 
   above if you have not already.

2. Build and push your image to the location specified by `IMG`:
   ```
   make docker-build docker-push IMG=<some-registry>/whalesay-operator:tag
   ```
	
3. Deploy the controller to the cluster with the image specified by `IMG`:
   ```
   make deploy IMG=<some-registry>/whalesay-operator:tag
   ```

*Note:* to remote the controller from the cluster later, use `make undeploy`.

### Directory Structure
Some important folders:

- `/api`: Directories that map to the Golang definitions for CRDs and supporting code
- `/config`: The Kubernetes manifests provided and managed by Kubebuilder and 
  its tools.  These files should not be directly edited.
   - `/config/crd`: The auto-generated CRD manifests.
   - `/config/manager`: Auto-generated manifests needed for deployment of the
     operator binary.
   - `/config/rbac`: Auto-generated RBAC manifests needed to allow controllers
     to function (see 
     [here](https://book.kubebuilder.io/reference/markers/rbac.html) for more 
     information).
   - `/config/default`: The "root" `kustomization.yaml` file and patche that 
     includes all the others.
- `/controllers`: Golang code for the controller component of the operator

## Project Scaffold
For reference the below commands were used to bootstrap the original project
scaffold:
```
go mod init arqtan.io/k8s-operator-example
kubebuilder init --domain arqtan.io
kubebuilder create api --kind HelloWorld --version v1 --controller --resource
```
Its worth noting some scaffold files have been edited since (like `.gitignore`
and `Dockerfile`) but crucially, the project layout has stayed the same to
retain compatability with future `kubebuilder` and `operator-sdk` commands.

## License
Copyright 2023. All rights reserved. 
Arqtan.io Ltd.