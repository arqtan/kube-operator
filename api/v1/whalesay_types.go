/*
Copyright 2023 Arqtan.io Limited. All rights reserved.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// WhaleSaySpec defines the desired state of WhaleSay
type WhaleSaySpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of WhaleSay. Edit whalesay_types.go to remove/update
	Msg string `json:"msg,omitempty"`
}

// WhaleSayStatus defines the observed state of WhaleSay
type WhaleSayStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// WhaleSay is the Schema for the whalesays API
type WhaleSay struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   WhaleSaySpec   `json:"spec,omitempty"`
	Status WhaleSayStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// WhaleSayList contains a list of WhaleSay
type WhaleSayList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []WhaleSay `json:"items"`
}

func init() {
	SchemeBuilder.Register(&WhaleSay{}, &WhaleSayList{})
}
