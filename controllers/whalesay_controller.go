/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	arqtaniov1 "arqtan.io/k8s-operator-example/api/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WhaleSayReconciler reconciles a WhaleSay object
type WhaleSayReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=arqtan.io,resources=whalesays,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=arqtan.io,resources=whalesays/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=arqtan.io,resources=whalesays/finalizers,verbs=update

func (r *WhaleSayReconciler) CreateWhaleSayJob(ctx context.Context, inst *arqtaniov1.WhaleSay) (*batchv1.Job, error) {
	var err error

	// Child Job struct, raw Golang instead of marshalled YAML
	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      inst.Name + "-job",
			Namespace: inst.Namespace,
		},
		Spec: batchv1.JobSpec{
			Template: corev1.PodTemplateSpec{
				Spec: corev1.PodSpec{
					RestartPolicy: corev1.RestartPolicyNever,
					Containers: []corev1.Container{
						{
							Name:            "whalesay-container",
							ImagePullPolicy: corev1.PullIfNotPresent,
							Image:           "docker/whalesay:latest",
							Command:         []string{"cowsay"},
							Args:            []string{inst.Spec.Msg},
						},
					},
				},
			},
		},
	}

	// Associate this controller with job resource
	err = ctrl.SetControllerReference(inst, job, r.Scheme)
	if err != nil {
		return &batchv1.Job{}, err
	}

	// Create the actual job on the cluster
	err = r.Create(ctx, job)
	if err != nil {
		return &batchv1.Job{}, err
	}

	return job, nil
}

// Reconcile function responds to events for the HelloWorld CRD
func (r *WhaleSayReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := log.FromContext(ctx)
	var err error

	// Get triggering instance
	inst := &arqtaniov1.WhaleSay{}
	err = r.Client.Get(ctx, req.NamespacedName, inst)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		// Error reading object, requeue
		return ctrl.Result{}, err
	}

	// Look for existing job
	job := &batchv1.Job{}
	findParams := types.NamespacedName{
		Name:      inst.Name + "-job",
		Namespace: inst.Namespace,
	}
	err = r.Client.Get(ctx, findParams, job)
	if err != nil {
		if errors.IsNotFound((err)) {

			// Create new child job
			job, err = r.CreateWhaleSayJob(ctx, inst)
			if err != nil {
				return ctrl.Result{}, err
			}
			log.Info("Child Job resource created on-cluster",
				"Name", inst.Name+"-job", "Namespace", inst.Namespace)

		} else {

			// Actual error
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *WhaleSayReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&arqtaniov1.WhaleSay{}).
		Complete(r)
}
